
public class LookAndSay {
  
  public static void main(String[] args) {
    
   System.out.println(sayPyramid(111221,3)); 
    
  }
 
  
  
  private static int say (int num, boolean tre) {
   return Integer.parseInt(say(num)); 
  }
  
  public static String say (int num) {
  
    
    String intString = Integer.toString(num);
    int previous = intString.charAt(0) - '0';
    int runningCount = 1;
    StringBuilder sr = new StringBuilder();
    
    
    
    
    for (int curr : (intString.substring(1)).toCharArray()){
    
      curr = curr - '0';
    
      
      if (curr == previous){
        runningCount++;
      } else {
       sr.append(runningCount + "" + previous);
       runningCount = 1;
       
      }
      
      
      previous = curr;
    }
    
    sr.append(runningCount + "" + previous);
    
    
    
    return sr.toString();
  }
  
  public static String sayPyramid(int num, int layers){
    
    num  = say(num, false);
    StringBuilder str = new StringBuilder(num + "\n");
    
    
    
    
    for (int i = 1; i < layers; i++) {
      str.append((num = say(num, false)) + "\n");
    }
    
    return str.toString();
  }
    
    
  
}